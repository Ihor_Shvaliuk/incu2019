
#Necessary libraries in order to work with API
import requests
import json


online_nexus = {"ip": "sbx-nxos-mgmt.cisco.com", 
                "port": "80", 
                "user":"", 
                "pass":""}


uri = 'http://{}:{}/ins'.format(online_nexus['ip'], online_nexus['port'])

jsonrpc_headers = {'Content-Type': 'application/json-rpc'}

#Authentication strings
online_nexus["user"] = input("User: ")
online_nexus["pass"] = input("Password: ")

class nexus():
  
  #Payload for getting version number and type of platform
  payload_get_version = [
        {
          "jsonrpc": "2.0",
          "method": "cli",
          "params": {
            "cmd": "show version",
            "version": 1
          },
          "id": 1
        }
      ]
  version_responce = requests.post(uri, 
                           data=json.dumps(payload_get_version),
                           headers=jsonrpc_headers, 
                           auth=(online_nexus["user"], online_nexus["pass"]))

  version_platform = json.loads(version_responce.text)
  version = "Version is " + version_platform["result"]["body"]["kickstart_ver_str"]   
  platform = "Platform is " + version_platform["result"]["body"]["chassis_id"] 
  


  
  #Method for getting int status
  def get_interface_status(self, if_name):

    #Payload for getting int status
    payload_get_interface_status = [
      {
        "jsonrpc": "2.0",
        "method": "cli",
        "params": {
           "cmd": "show interface ",
           "version": 1
        },
        "id": 1
      }
     ]

    

    payload_get_interface_status[0]["params"]["cmd"] += if_name   
    response = requests.post(uri,
                        data=json.dumps(payload_get_interface_status),
                        headers=jsonrpc_headers, 
                        auth=(online_nexus["user"], online_nexus["pass"]))
    response_dic = json.loads(response.text)   

    return response_dic["result"]["body"]["TABLE_interface"]["ROW_interface"]["state"]
  

  #Method for getting int description
  def configure_interface_desc(self, if_name, if_desc):

    #Method for getting int description
    payload_interface_desc = [
      {
        "jsonrpc": "2.0",
        "method": "cli",
        "params": {
           "cmd": "interface ",
           "version": 1
        },
        "id": 1
        },
        {
        "jsonrpc": "2.0",
        "method": "cli",
        "params": {
           "cmd": "description ",
           "version": 1
        },
        "id": 2
      },
        {
        "jsonrpc": "2.0",
        "method": "cli",
        "params": {
           "cmd": "show interface ",
           "version": 1
        },
        "id": 3
      }
     ]
     
    payload_interface_desc[0]["params"]["cmd"] += if_name 
    payload_interface_desc[1]["params"]["cmd"] += if_desc
    payload_interface_desc[2]["params"]["cmd"] += if_name
    payload_interface_desc[2]["params"]["cmd"] += " description"
    
    response = requests.post(uri,
                        data=json.dumps(payload_interface_desc),
                        headers=jsonrpc_headers, 
                        auth=(online_nexus["user"], online_nexus["pass"]))
    
    response_dictionary = json.loads(response.text)    
    return response_dictionary[2]["result"]["body"]["TABLE_interface"]["ROW_interface"]["desc"]



