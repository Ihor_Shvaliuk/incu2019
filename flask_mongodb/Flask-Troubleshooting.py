from flask import Flask
from flask import abort
from flask import Response
from flask import request
import json
app = Flask(__name__)


authorized_users=['agata','tristan','peter','joanna']
@app.route("/hello/<username>")
def hello(username):
    return "Hello World %s !" % username.title()

@app.route("/authorized_only/<usrname>")
def authorized_only(usrname):
	if usrname.lower() not in authorized_users :
		return abort(500, 'The user %s is not authorized to view the page' % usrname.title())
	else:
		return 'Welcome to your personal page, %s ' % usrname.title()

app.run(port=7676)
