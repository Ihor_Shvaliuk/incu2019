from flask import Flask 
from flask import jsonify
from flask import request
from flask import abort


app = Flask(__name__)

library = [
         {
                'Userid' : 1,
                'Name' : 'Serhii',
                'Phone number' : '0963193981',
                'email' : 'shvaluk95@gmail.com'
        },
        {
                'Userid' : 2,
                'Name' : 'Vlad',
                'Phone number' : '0963193981',
                'email' : 'shvaluk95@gmail.com'
        },
        {
                'Userid' : 3,
                'Name' : 'Ihor',
                'Phone number' : '0963193981',
                'email' : 'shvaluk95@gmail.com'
        }
]



@app.route('/library', methods=['GET'])
def ge_tall():
        return jsonify({"library": library})

@app.route('/library/add', methods=['POST'])
def add():
        payload = request.json
        library.append(payload)
        return "You've add : {} \n".format(payload)

@app.route('/library/<int:id>', methods = ['GET'])
def get_one(id): 
        lib = [item for item in library if item['Userid'] == id]
        if not lib:
                abort(400)
                
        return jsonify ({'User': lib})


@app.route('/library/update/<int:id>', methods=['PUT'])
def update(id):
        lib = [item for item in library if item['Userid'] == id]
        if not lib:
                abort(400)
        for i in library:
                if i['Userid'] == id:
                        i['Name'] = request.json.get('Name', i['Name'])
                        i['Phone number'] = request.json.get('Phone number', i['Phone number'])
                        i['email'] = request.json.get('email', i['email'])
        return jsonify({'updated_library': i})
            

@app.route('/library/delete/<int:id>', methods=['DELETE'])
def delete_entry(id):
        lib = [item for item in library if item['Userid'] == id]
        if not lib:
                abort(400)
        for i in library:
                if i['Userid'] == id:
                        library.remove(i)
        return jsonify({"You've deleted" : lib})
       
app.run(port=7676, debug=True)


