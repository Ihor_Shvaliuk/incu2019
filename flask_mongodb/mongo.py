#Necessary libraries
from pymongo import MongoClient
from bson.json_util import dumps

#function for connecting to database
def get_mongo():
    client   = MongoClient("localhost", 27017)
    db = client["incubator"]
    return db['working address book']

#function for inserting new entry to DB 
def insert_new_entry(dict):
    db = get_mongo()
#here must be insert_one but in order to check script I`ve put insert_many 
    action = db.insert_many(dict)

#function for updating entries in DB 
def update_entry(firstname, lastname, dict):
    db = get_mongo()
    action = db.update_many({"First Name" : str(firstname) , "Last Name" : str(lastname)}, dict)

#function for displaying entry or all entries in DB
def display_entries(firstname = None, lastname = None):
    db = get_mongo()
    if firstname != None and lastname != None:
        action = db.find({"First Name" : {"$regex" : str(firstname)} , "Last Name" : {"$regex": str(lastname)}})
    else:
        action = db.find()
        action = sorted(action, key = lambda l: l["Last Name"])

    for entries in action:
         print (dumps(entries))

#function for deleting entry or all entries in DB
def delete_entries(firstname = None, lastname = None):
    db = get_mongo()
    if firstname != None and lastname != None:
        action = db.delete_many({"First Name" : str(firstname) , "Last Name" : str(lastname)})
    else:
        action = db.delete_many({})



#Checking 

address_book = [
{	
    "First Name" : "Ihor", 
    "Last Name" : "Shvaliuk",
    "Email address" : "shvaliuk95@gmail.com",
    "Home phone number" : "0666027500",
    "Work phone number" : "0963193981"
},
{
    "First Name" : "Ihor", 
    "Last Name" : "Avaliuk",
    "Email address" : "shvaliuk95@gmail.com",
    "Home phone number" : "0666027500",
    "Work phone number" : "0963193981"
},
{
    "First Name" : "Ihor", 
    "Last Name" : "Chvaliuk",
    "Email address" : "shvaliuk95@gmail.com",
    "Home phone number" : "0666027500",
    "Work phone number" : "0963193981"
}
]

update = {"$set":{"Home phone number":"937-99-92"}}

firstname = "Ihor"
secondname = "Chvaliuk"


delete_entries()

insert_new_entry(address_book)

update_entry(firstname, secondname, update)


display_entries()



