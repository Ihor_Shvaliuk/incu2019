
###################################################
# IP address validator 
# details: https://en.wikipedia.org/wiki/IP_address
# Student should enter function on the next lines.
# 
# 
# First function is_valid_ip() should return:
# True if the string inserted is a valid IP address
# or False if the string is not a real IP
# 
# 
# Second function get_ip_class() should return a string:
# "X is a class Y IP" or "X is classless IP" (without the ")
# where X represent the IP string, 
# and Y represent the class type: A, B, C, D, E
# ref: http://www.cloudtacker.com/Styles/IP_Address_Classes_and_Representation.png
# Note: if an IP address is not valid, the function should return
# "X is not a valid IP address"
###################################################

import re

def is_valid_ip(ip):
    if re.match(r"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", ip):
        return True
    else:
        return False


def get_ip_class(ip):
    if is_valid_ip(ip):
        get_ip_1st_octet = ip.split(".")
        if int(get_ip_1st_octet[0]) >= 0 and int(get_ip_1st_octet[0]) <= 127:
            return (ip + " is a class A IP")
        elif int(get_ip_1st_octet[0]) >= 128 and int(get_ip_1st_octet[0]) <= 191:
            return (ip + " is a class B IP")
        elif int(get_ip_1st_octet[0]) >= 192 and int(get_ip_1st_octet[0]) <= 223:
            return (ip + " is a class C IP")
        elif int(get_ip_1st_octet[0]) >= 224 and int(get_ip_1st_octet[0]) <= 239:
            return (ip + " is a class D IP")
        else:
            return (ip + " is a class E IP")
    else:
        return (ip + " is not a valid IP address")


#Checking 
while True:
    ip = input("Enter IP-Address here: ")
    is_valid_ip(ip)
    print (get_ip_class(ip))



